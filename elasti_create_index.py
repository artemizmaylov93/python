#!/usr/bin/env python3
import os
import string
from elasticsearch import Elasticsearch

ESIP = "192.168.0.144"
ESPORT = 9200
INDEX = "logstash"
LASTDAY = 2019.01

i = 1
while i <= 31:
    if i < 10:
        date = "0%s" % i
    else:
        date = i
    esclient = Elasticsearch(['%s:%s' % (ESIP, ESPORT)], timeout=30)
    result = esclient.indices.create(
        index=('%s-%s.%s' % (INDEX, LASTDAY, date)),
        body={
            "settings" : {
                "index" : {
                    "number_of_replicas" : 0,
                    "refresh_interval" : -1
                }
            }
        }
    )
    i = i + 1
    print(result)