#!/usr/bin/env python3
import os
import string
from elasticsearch import Elasticsearch

ESIP = "192.168.0.144"
ESIPOLD = "90.189.192.248"
ESPORT = 9200
INDEX = "logstash"
LASTDAY = 2019.01

i = 15
while i <= 31:
    if i < 10:
        date = "0%s" % i
    else:
        date = i
    esclient = Elasticsearch(['%s:%s' % (ESIP, ESPORT)], timeout=1900)
    result = esclient.reindex(
        body={
            "conflicts": "proceed",
            "source": {
                "remote": {
                    "host": "http://%s:%s" % (ESIPOLD, ESPORT)
                },
                "index": "%s-%s.%s" % (INDEX, LASTDAY, date),
                "type": ["logs", "nginx_error"],
                "size": 10000
            },
            "dest": {
                "index": "%s-%s.%s" % (INDEX, LASTDAY, date),
                "type": "logs"
            },
            "script": {
                "inline": "if (ctx._source.timestamp != '') {ctx._source.remove('timestamp')}",
                "lang": "painless"
            }
        }
    )
    print("%s-%s.%s" % (INDEX, LASTDAY, date), '\n', result)
    i = i + 1