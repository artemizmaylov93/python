# -*- coding: utf-8 -*-
money = 2000
if money > 1000:
    print ("I'm rich")
else:
    print ("I'm poor")

twinkies = 444
if twinkies < 100:
    print("few")
elif twinkies > 500:
    print("much")
else:
    print("fine")

n = 4444
if n >= 100 and n <= 500:
    print ("little")
elif n >= 1000 and n <= 5000:
    print ("much")
else:
    print ("I do not know")

#ninjas = 27
#if ninjas < 50 and ninjas > 30:
#    print('Ix mnogo')
#elif ninjas < 30 and ninjas > 10:
#    print('No ya sparvlus')
#elif ninjas < 10:
#    print('Winn')
#else:
#    print('XZ')

ninjas = 5
if ninjas > 50:
    print('AAAAHTUNG')
if ninjas < 50 and ninjas > 30:
    print("a lot of them")
elif ninjas < 30 and ninjas > 10:
    print("but I can handle")
elif ninjas < 10:
    print("victory")
